import React, {useState, useEffect} from "react";
import {Button, Form, Modal, Spinner} from "react-bootstrap";
import {Typeahead} from "react-bootstrap-typeahead";

import 'react-bootstrap-typeahead/css/Typeahead.css';

async function postData(data){
    try {
        const response = await fetch("http://localhost:3001/api/streams/",{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return await response.json();
    }catch{
        //console.error("Error");
    }
}

function AddStream(props){const [show, setShow] = useState(false);
    const group=props.group;

    const teachers=props.allTeachers;
    const currentDisciplines=props.disciplines;
    const allDisciplines=props.allDisciplines;

    const [load, setLoad] = useState(false);

    const [teacher, setTeacher] = useState([]);
    const [discipline, setDiscipline] = useState([]);

    const [year, setYear] = useState("");
    const [term, setTerm] = useState("o");

    const [disciplineMessage, setDisciplineMessage] = useState("");
    const [teacherMessage, setTeacherMessage] = useState("");
    const [yearMessage, setYearMessage] = useState("");

    const [availableDisciplines, setAvailableDisciplines] = useState([]);

    const handleClose = () => {
        setShow(false);
        setDiscipline([]);
        setTeacher([])
    };
    const handleShow = () => {
        props.handleUpdated();

        setShow(true);
        setLoad(false);
    }

    useEffect(()=>{
        if (show){
            let curDiscpl=[];
            if (currentDisciplines.length !== 0){
                currentDisciplines.map(i=>
                    curDiscpl.push(i.discipline_id));
                setAvailableDisciplines(allDisciplines.filter(i => !curDiscpl.includes(i.discipline_id)));
            }else{
                setAvailableDisciplines(allDisciplines);
            }
        }

    },[show, allDisciplines, teachers])

    const handleLoad = () => {
        setLoad(true);
        //handleClose();
        handleCheck();
    }

    const handleCheck = () => {
        let fl1, fl2, fl3;
        if (! discipline[0] ) {
            setDisciplineMessage("Выберите дисциплину");
            setLoad(false);
            fl1=false;
        } else {
            setDisciplineMessage("");
            fl1=true;
        }

        if (!teacher[0]){
            setTeacherMessage("Выберите преподавателя");
            setLoad(false);
            fl2=false;
        } else {
            setTeacherMessage("");
            fl2=true;
        }

        if (!year){
            setYearMessage("Введите год");
            setLoad(false);
            fl3=false;
        }else{
            if (year.length === 4 && year.match(/2[0-9][0-9][0-9]/)) {
                setYearMessage("");
                fl3=true;
            }else{
                setYearMessage("Введите корректный год");
                setLoad(false);
                fl3=false;
            }
        }

        if (fl1 && fl2 && fl3){
            const data = {
                group_id: group.group_id,
                discipline_id: discipline[0].discipline_id,
                teacher_id: teacher[0].teacher_id,
                stream_year: year,
                term: term
            };
            setLoad(false);
            postData(data).then(()=>{
                //console.log(res);
                handleClose();
                //window.location.reload();
                props.handleUpdated();
            });
        }
    }

    return(
        <>
            <Button variant="outline-primary" onClick={handleShow}>Добавить поток</Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                style={{paddingTop: "2rem"}}
                size="lg"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Сформировать поток</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <div style={{display: "flex"}}>
                        <p >Группа: </p>
                        <p style={{fontWeight: "bold", marginLeft: "10px", }}>{group.group_name}</p>
                    </div>
                    <Form>
                        <Form.Group controlId="discipline" style={{marginBottom: "20px"}}>
                            <Form.Label>Дисциплина:</Form.Label>
                            <Typeahead
                                disabled={availableDisciplines === []}
                                id="set-discipline"
                                labelKey={option => `${option.discipline_name}`}
                                onChange={setDiscipline}
                                options={availableDisciplines}
                                placeholder="Дициплина"
                                selected={discipline}
                            />
                            <Form.Text>
                                {disciplineMessage}
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="teacher" style={{marginBottom: "20px"}}>
                            <Form.Label>Преподаватель:</Form.Label>
                            <Typeahead
                                disabled={availableDisciplines === []}
                                id="set-teacher"
                                labelKey={option => `${option.teacher_name}`}
                                onChange={setTeacher}
                                options={teachers}
                                placeholder="Преподаватель"
                                selected={teacher}
                            />
                            <Form.Text>
                                {teacherMessage}
                            </Form.Text>
                        </Form.Group >
                        <div style={{display: "flex"}}>
                            <Form.Group controlId="year" style={{marginBottom: "20px", marginRight: "30px"}}>
                                <Form.Label>Год обучения:</Form.Label>
                                <Form.Control style={{width: "120px"}}
                                              type="text"
                                              placeholder="Год"
                                              disabled={availableDisciplines === []}
                                              value={year}
                                              onChange={(e)=>{
                                                  setYear(e.target.value);
                                                  setYearMessage("");
                                              }}
                                />

                                <Form.Text>
                                    {yearMessage}
                                </Form.Text>
                            </Form.Group>
                            <Form.Group controlId="term">
                                <Form.Label>Семестр:</Form.Label>
                                <Form.Control style={{width: "120px"}}
                                              type="text"
                                              placeholder="Семестр"
                                              disabled={availableDisciplines === []}
                                              as="select"
                                              value={term}
                                              onChange={(e)=>{setTerm(e.target.value)}}

                                >
                                    <option value='o'>Осень</option>
                                    <option value='s'>Весна</option>
                                </Form.Control>
                            </Form.Group>
                        </div>

                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addStream" variant="outline-primary" onClick={handleClose}> Отмена </Button>
                    {load ?
                        (
                            <Button variant="primary">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                <span className="visually-hidden">Loading...</span>
                            </Button>
                        ) :
                        (<Button form="addStream" variant="primary" onClick={handleLoad}>Добавить</Button>)
                    }
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddStream;
