import React, {useState} from "react";
import {Form} from "react-bootstrap";

async function deleteClass(classId){
    try {
        const response = await fetch(`http://localhost:3001/api/classes/${classId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

async function postData(data){
    try {
        const response = await fetch("http://localhost:3001/api/assignments",{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return await response.json();
    }catch{
        //console.error("Error");
    }
}

function AddClassTask(props) {
    const tasks = props.tasks;
    const [add, setAdd] = useState(false);

    const [taskAdd, setTaskAdd] = useState("");

    const handleAdd = () => {
        if (tasks.length !== 0){
            setAdd(true);
            if (tasks) setTaskAdd(tasks[0].task_id);
        }
    }

    const handleCheck = () => {
        //console.log(props.classToAdd.class_id);
        //console.log(taskAdd);
        //props.handleUpdated();
        //setAdd(false);
        const data = {
            task_id: taskAdd,
            class_id: props.classToAdd.class_id
        }
        postData(data).then(()=>{
            props.handleUpdated();
        })
        //console.log(JSON.stringify(data));
    }

    const handleDelete = () => {
        deleteClass(props.classToAdd.class_id).then(()=>{
            props.handleUpdated();
        })
    }

    return(
        <>
            {add ? (
                    <div style={{display: "flex", alignItems: "center"}}>
                        <Form style={{width: "250px", marginRight: "10px"}}>
                            <Form.Control as="select"
                                          type="text"
                                          placeholder="Задание"
                                          value={taskAdd}
                                          onChange={(e)=>{setTaskAdd(e.target.value)}}
                            >
                                {tasks.map((item, id)=>{
                                        return(
                                            <option key={id} value={item.task_id}>{item.task_description}</option>
                                        )
                                    })}
                            </Form.Control>
                        </Form>
                        <div className="button-cross"
                             style={{color: "rgb(13 110 253 / 40%)"}}
                             onClick={handleCheck}
                        >
                            <i className="fas fa-check"/>
                        </div>
                        <div className="button-cross"
                             style={{color: "rgb(13 110 253 / 20%)", marginLeft: "10px"}}
                             onClick={()=>setAdd(false)}
                        >
                            <i className="fas fa-times"/>
                        </div>
                    </div>

                ) : (
                    <div style={{display: "flex"}}>
                        <div className="button-cross"
                             style={{color: "rgb(13 110 253 / 20%)"}}
                             onClick={handleAdd}
                        >
                            <i className="fas fa-plus"/>
                        </div>

                        <div className="button-cross"
                             style={{color: "rgb(13 110 253 / 50%)", marginLeft: "10px", cursor: "pointer"}}
                            onClick={handleDelete}
                        >
                            <p style={{marginBottom: "0", fontSize: "10px"}}>Удалить занятие</p>
                        </div>

                    </div>
                )
            }
        </>
    )
}
export default AddClassTask;