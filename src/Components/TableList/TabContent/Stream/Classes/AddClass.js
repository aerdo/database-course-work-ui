import {Button, Form, Spinner} from "react-bootstrap";
import React, {useState} from "react";

async function postData(data){
    try {
        const response = await fetch("http://localhost:3001/api/classes",{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return await response.json();
    }catch{
        //console.error("Error");
    }
}

function AddClass(props){
    const [classType, setClassType] = useState("p");
    const [date, setDate] = useState("");
    const [load, setLoad] = useState(false);

    const [dateMessage, setDateMessage] = useState("");

    const handleAdd = () => {
        setLoad(true);
        if (!date){
            setDateMessage("Поле не должно быть пустым");
            setLoad(false);
        }else{
            setDateMessage("");
            if (date.match(/^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((20)\d{2})\s*$/)){
                const data = {
                    stream_id: props.streamId,
                    class_date: getReverse(date),
                    class_type: classType
                }
                postData(data).then(()=>{
                    props.handleUpdated();
                    setLoad(false);
                    setDate("");
                    setClassType("p");
                });
            }else{
                setLoad(false);
                setDateMessage("Введите верную дату (дд.мм.гггг)")
            }
        }

    }

    function getReverse(s){
        let t = s.split(".");
        t = t.reverse();
        return t[0] + "-" + t[1] + "-" + t[2];
    }

    return(
        <>
            <Form style={{display: "flex", height: "62px"}}>
                <Form.Group style={{marginRight: "10px"}}>
                    <Form.Control style={{width: "210px"}}
                                  type="text"
                                  placeholder="Дата"
                                  disabled={load}
                                  onChange={(e)=>{
                                      setDate(e.target.value);
                                      setDateMessage("");
                                  }}
                    />
                    <Form.Text>{dateMessage}</Form.Text>
                </Form.Group>
                <Form.Group style={{marginRight: "10px"}}>
                    <Form.Control style={{width: "400px"}}
                                  type="text"
                                  placeholder="Тип занятия"
                                  as="select"
                                  disabled={load}
                                  value={classType}
                                  onChange={(e)=>{setClassType(e.target.value)}}
                    >
                        <option value="l">Лабораторное занятие</option>
                        <option value="p">Практическое занятие</option>
                        <option value="k">Консультация</option>
                    </Form.Control>

                </Form.Group>
                {load ?
                    (
                        <Button style={{height: "38px"}}>
                            <Spinner
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />
                        </Button>
                    ) :
                    (<Button style={{height: "38px"}} onClick={handleAdd}>Добавить</Button>)
                }

            </Form>
        </>
    )
}
export default AddClass;