import React, {useState, useEffect} from "react";
import AddClassTask from "./AddClassTask";
import AddClass from "./AddClass";

async function deleteTaskFromClass(planId){
    try {
        const response = await fetch(`http://localhost:3001/api/assignments/${planId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function Classes(props){
    const streamId = props.stream;
    const [classes, setClasses] = useState([]);
    const [assignments, setAssignments] = useState([]);
    const [tasks, setTasks] = useState([]);
    const [updated, setUpdated] = useState(false);

    const handleUpdated = () =>{
        setUpdated(true);
    }

    async function fetchClasses() {
        try {
            const response = await fetch(`http://localhost:3001/api/streams/${streamId}/classes`);
            const json = await response.json();
            setClasses(json);
        }catch{

        }
    }

    async function fetchTasks() {
        try {
            const response = await fetch(`http://localhost:3001/api/streams/${streamId}/assignments/spare`);
            const json = await response.json();
            setTasks(json);
        }catch{

        }
    }

    async function fetchAssignments() {
        try {
            const response = await fetch(`http://localhost:3001/api/streams/${streamId}/assignments`);
            const json = await response.json();
            setAssignments(json);
        }catch{

        }
    }

    useEffect(()=>{
        if (classes.length === 0){
            fetchClasses().then(()=>{
                fetchAssignments().then(()=>{
                    fetchTasks().then();
                });
            });

        }
        if (updated){
            fetchClasses().then(()=>{
                fetchAssignments().then(()=>{
                    fetchTasks().then();
                });
            });
            setUpdated(false);
        }
    }, [classes.length,updated])

    function getReverse(s){
        let t = s.split("-");
        t = t.reverse();
        let s1 = t[0]+"."+t[1]+"."+t[2];
        return s1;
    }

    const handleDelete = (id) => {
        deleteTaskFromClass(id).then(()=>{
                setUpdated(true);
            }
        );

    }

    return(
        <>
            <div style={{margin: "0 40px 0 40px",height: "320px", overflowY: "auto"}}>
                {classes && classes.map((item, id)=>{
                    const a = assignments.find(i=>i.class_id === item.class_id);
                    return(
                        <div key={id} style={{width: "100%",
                            display: "flex",
                            padding: "10px 20px 10px 20px ",
                            border: "1px solid rgba(0,0,0,.125)",
                            borderRadius: "0.25rem",
                            marginBottom: "10px",
                            alignItems: "center",
                            height: "70px"
                        }}>
                            <p style={{marginRight: "10px", marginBottom: "0"}}>{id+1}.</p>
                            <p style={{marginRight: "10px", marginBottom: "0", fontWeight: "500"}}>{getReverse(item.class_date)}</p>
                            <p style={{marginRight: "10px", marginBottom: "0"}}>{item.class_type === 'p' ?
                                'Практическое занятие' :
                                item.class_type === 'l' ?
                                    'Лабораторное занятие' :
                                    'Консультация'}
                            </p>

                            <div>
                                {a ?
                                    (<div style={{display: "flex"}}>
                                        <p style={{marginRight: "10px", marginBottom: "0", fontStyle: "italic"}}>
                                            {a.task_description}

                                        </p>
                                        <div className="button-cross"
                                             style={{color: "rgb(13 110 253 / 60%)"}}
                                             onClick={()=>handleDelete(a.plan_id)}
                                        >
                                            <i className="fas fa-minus"/>
                                        </div>
                                    </div>)
                                    :
                                    (
                                        <AddClassTask classToAdd={item}
                                                          streamId={streamId}
                                                          tasks={tasks}
                                                          handleUpdated = {()=>handleUpdated()}
                                            />
                                )}
                            </div>

                        </div>
                    )
                })}
            </div>
            <div style={{margin: "10px 40px 0 40px"}}>
                <h6>Добавить занятие:</h6>
                <AddClass streamId={streamId} handleUpdated={()=>handleUpdated()}/>
            </div>
        </>

    )
}

export default Classes;