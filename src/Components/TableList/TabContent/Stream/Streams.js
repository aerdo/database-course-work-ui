import React, {useEffect, useState} from "react";
import StreamsByGroup from "./StreamsByGroup";

function Streams(){
    const [groups, setGroups] = useState([]);
    const [disciplines, setDisciplines] = useState([]);
    const [teachers, setTeachers] = useState([]);

    const [updated, setUpdated] = useState(false);
    const handleUpdated = () =>{
        setUpdated(true);
    }

    async function fetchGroups() {
        try {
            const response = await fetch("http://localhost:3001/api/groups")
            const json = await response.json();
            setGroups(json);
        }catch{

        }
    }
    async function fetchDisciplines() {
        try {
            const response = await fetch("http://localhost:3001/api/disciplines")
            const json = await response.json();
            setDisciplines(json);
        }catch{

        }
    }

    async function fetchTeachers() {
        try {
            const response = await fetch("http://localhost:3001/api/teachers")
            const json = await response.json();
            setTeachers(json);
        }catch{

        }
    }


    useEffect(() => {
        if (groups.length === 0) {
            fetchGroups().then();
            fetchDisciplines().then();
            fetchTeachers().then();

        }
        else if (updated){
            setUpdated(false);
            fetchGroups().then();
            fetchDisciplines().then();
            fetchTeachers().then();
        }
    },[updated]);


    return(
        <div style={{}}>
            <div style={{marginBottom: "20px"}}>
                <h4 style={{display: "inline-block", verticalAlign: "top", marginBottom: "0"}}>Потоки</h4>

            </div>
            <div style={{height: "580px", overflowY: "auto"}}>
                {groups.map((item,id)=>{
                    return(
                        <StreamsByGroup group={item}
                                        key={id}
                                        disciplines={disciplines}
                                        teachers={teachers}
                                        handleUpdated={()=>handleUpdated()}
                        />
                    )
                })}
            </div>

        </div>
    )
}

export default Streams;