import AddStream from "./AddStream";
import React, {useEffect, useState} from "react";
import {Accordion, Table} from "react-bootstrap";
import StreamReview from "./StreamReview";

function StreamsByGroup(props){
    const allDisciplines=props.disciplines;
    const allTeachers=props.teachers;
    const [disciplines, setDisciplines] = useState([]);
    const group = props.group;

    const [updated, setUpdated] = useState(false);

    const handleUpdated = () =>{
        setUpdated(true);
        props.handleUpdated();
    }

    async function fetchDisciplines() {
        try {
            const response = await fetch(`http://localhost:3001/api/groups/${group.group_id}/disciplines`)
            return await response.json();
        }catch{

        }
    }

    useEffect(() => {
        if (disciplines.length === 0) {
            fetchDisciplines().then((res) => {
                if (res.length !== 0) {
                    let x = [];
                    res.map(item => x.push(item.stream_year));
                    setDisciplines(res);
                } else {
                    setDisciplines(res);
                }
            });
        }
        if (updated){
            handleUpdated();
            fetchDisciplines().then((res) => {
                if (res.length !== 0) {
                    let x = [];
                    res.map(item => x.push(item.stream_year));
                    setDisciplines(res);
                } else {
                    setDisciplines(res);
                }
            });
            setUpdated(false);
        }
    },[updated, disciplines.length]);

    return(
        <Accordion style={{margin: "5px 5px 20px 5px"}}>
            <Accordion.Item eventKey={(group.group_id).toString()}>
                <Accordion.Header>
                    <h5 style={{marginBottom: "0"}}>{group.group_name}</h5>
                </Accordion.Header>
                <Accordion.Body>
                    {disciplines.length === 0 ? (
                        <h6 style={{marginBottom: "10px"}}>Пусто</h6>
                    ) : (
                        <Table hover size="sm">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Год</th>
                                <th>Дисциплина</th>
                                <th>Преподаватель</th>
                                <th>Семестр</th>
                            </tr>
                            </thead>
                            <tbody>
                            {disciplines.map((item, id)=>{
                                return(
                                    <StreamReview item={item} key={id} handleUpdated={()=>handleUpdated()}/>
                                )
                            })}

                            </tbody>
                        </Table>
                    )}

                    <AddStream
                        group={group}
                        disciplines={disciplines}
                        allDisciplines={allDisciplines}
                        allTeachers={allTeachers}
                        handleUpdated={()=>handleUpdated()}
                    />
                </Accordion.Body>
            </Accordion.Item>
        </Accordion>
    )
}
export default StreamsByGroup;