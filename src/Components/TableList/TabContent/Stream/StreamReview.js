import React, {useState} from "react";
import {Modal} from "react-bootstrap";
import Classes from "./Classes/Classes";

async function deleteStream(streamId){
    try {
        const response = await fetch(`http://localhost:3001/api/streams/${streamId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function StreamReview(props) {
    const item=props.item;
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
    };
    const handleShow = () => {
        setShow(true);
    }

    const [messageDelete, setMessageDelete] = useState("");
    const handleDelete = () =>{
        deleteStream(item.stream_id).then(res=>{
            if (res && res.message){
                setMessageDelete("Невозможно удалить поток");
                setTimeout(()=>setMessageDelete(""),1500);
            }else{
                setTimeout(handleClose,400);
                props.handleUpdated();
            }
        })
    }

    return(
        <>
            <tr onClick={handleShow}>
                <td>{item.stream_id}</td>
                <td>{item.stream_year}</td>
                <td>{item.discipline_name}</td>
                <td>{item.teacher_name}</td>
                <td>{item.term ==='o' ? 'Осень' : 'Весна' }</td>
            </tr>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                size="xl"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem", paddingBottom: "0"}}>
                    <div style={{textAlign: "center"}}>
                        <h4>{item.discipline_name}</h4>
                        <h6 style={{marginBottom: "0"}}>{item.teacher_name}</h6>
                        <div style={{display: "flex", justifyContent: "center"}}>
                            <p style={{marginRight: "10px"}}>{item.term ==='o' ? 'Осень' : 'Весна' }</p>
                            <p>{item.stream_year}</p>
                        </div>
                    </div>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem", paddingBottom: "1rem"}}>
                    <Classes stream={item.stream_id}/>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <div style={{ fontSize: "13px", display: "flex"}}>
                        <p style={{cursor: "pointer", color: "rgb(13 110 253 / 50%)", marginBottom: "0"}} onClick={handleDelete}>
                            Удалить поток
                        </p>
                        <p style={{marginLeft: "10px", color: "#0d6efd", marginBottom: "0"}}>{messageDelete}</p>
                    </div>
                </Modal.Footer>
            </Modal>
        </>

    )
}
/*<Tabs defaultActiveKey="classes">
                        <Tab eventKey="classes" title="Занятия">

                        </Tab>
                        <Tab eventKey="plan" title="Учебный план">
                            <Assignments stream={item.stream_id}/>
                        </Tab>
                    </Tabs>*/
export default StreamReview;