import React from "react";
import DisciplineInfo from "./DisciplineInfo";

function DisciplineByYear(props) {
    const arr = props.items.filter(i=>i.stream_year === props.year);
    const arr1 = arr.filter(i=>i.term === 'o');
    const arr2 = arr.filter(i=>i.term === 's');
    //
    return(
        <div>
            {arr2.length !==0 ? (
                <div>
                    <h5>Весна</h5>
                    <ul>
                        {arr2.map((item,id)=>{
                            return(
                                <li key={id}>
                                    <div style={{display: "inline-block", verticalAlign: "top", fontWeight: "600"}}>
                                        {item.discipline_name}
                                    </div>

                                    <div className="button-cross"
                                         style={{display: "inline-block",
                                             verticalAlign: "top",
                                             marginLeft: "10px",
                                             color: "rgb(13 110 253 / 40%)"}}>
                                        <DisciplineInfo discipline={item} group={props.group}/>
                                    </div>
                                    <div style={{marginBottom: "10px"}}>
                                        {item.teacher_name}
                                    </div>
                                </li>
                            )
                        }) }
                    </ul>
                </div>
            ): (<div/>)}


            {arr1.length !==0 ? (
                <div>
                    <h5>Осень</h5>
                    <ul>
                        {arr1.map((item,id)=>{
                            return(
                                <li key={id}>
                                    <div style={{display: "inline-block", verticalAlign: "top", fontWeight: "600"}}>
                                        {item.discipline_name}
                                    </div>

                                    <div className="button-cross"
                                         style={{display: "inline-block",
                                             verticalAlign: "top",
                                             marginLeft: "10px",
                                             color: "rgb(13 110 253 / 40%)"}}>
                                        <DisciplineInfo discipline={item} group={props.group}/>
                                    </div>
                                    <div style={{marginBottom: "10px"}}>
                                        {item.teacher_name}
                                    </div>
                                </li>
                            )
                        }) }
                    </ul>
                </div>
            ): (<div/>)}
        </div>
    )
}

export default DisciplineByYear;