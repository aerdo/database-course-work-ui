import React from "react";

async function updateAttendance(data){
    try {
        const response = await fetch(`http://localhost:3001/api/attendance/${data.att_id}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function AddAttendance(props){
    const handleYes = () => {
        const data = props.cur;
        data.presence = 1;
        updateAttendance(data).then(()=>{
            props.handleUpdated();
        });
    }

    const handleNo = () => {
        const data = props.cur;
        data.presence = 0;
        updateAttendance(data).then(()=>{
            props.handleUpdated();
        });
    }
    return(
        <>
            <div className="button-cross"
                 style={{color: "rgb(13 110 253 / 50%)"}}
                /*onClick={handleAdd}*/
                 /*onClick={()=>setAdd(true)}*/
            >
                <i style={{marginRight: "10px",cursor: "pointer"}}  className="fas fa-plus" onClick={handleYes}/>
                <i style={{cursor: "pointer"}} className="fas fa-minus" onClick={handleNo}/>
            </div>
        </>
    )
}
export default AddAttendance;