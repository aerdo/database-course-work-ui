import {useState, useEffect} from "react";



function Rating(props){
    const streamId=props.streamId;
    const students = props.students;
    const [data, setData] = useState([]);
    const [avgs, setAvgs] = useState([]);

    async function fetchRating(studentId, streamId) {
        try {
            const response = await fetch(`http://localhost:3001/api/asses/${studentId}/${streamId}`)
            const json = await response.json();
            //setAvgs(json);
            return json;
        }catch{

        }
    }

    useEffect(() => {
        /*let temp = [];
        students.forEach(i=>{
            fetchRating(i.student_id, streamId).then((res)=>{
                const t = i;
                t.avg = res[0].get_avg;
                temp.push(t);
                //data.push(t);
            })
        });
        setData(temp);
        */

        if (avgs.length === 0){
            let temp = [];
            students.forEach(i=>{
                fetchRating(i.student_id, streamId).then((res)=>{
                    const t = i;
                    t.avg = res.get_avg;
                    temp.push(t);
                    //data.push(t);
                })
            });
            setData(temp);
        }

    }, [students, streamId, avgs.length]);

    return(
        <>
            <ul style={{marginTop: "20px", marginLeft: "-10px"}}>
                {data.sort(function(a, b) {
                    return -a.avg + b.avg;
                }).map((item,id)=>{
                    return(
                        <li key={id} style={{listStyle: "none"}}>{id+1}. ({item.avg}) {item.student_name} </li>
                    )
                })}
            </ul>
        </>
    )
}
export default Rating;