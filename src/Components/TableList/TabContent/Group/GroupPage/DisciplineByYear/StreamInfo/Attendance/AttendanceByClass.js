import React, {useState, useEffect} from "react";
import AddAttendance from "./AddAttendance";

function AttendanceByClass(props){
    const curClass=props.class;
    const [att, setAtt] = useState([]);
    const [updated, setUpdated] = useState(false);
    const handleUpdated = () => {
        setUpdated(true);
    }

    useEffect(()=>{
        if (att.length === 0){
            fetchAtt().then();
        } else
        if (updated){
            fetchAtt().then();
            setUpdated(false);
        }
    },[att.length, updated, curClass])

    async function fetchAtt() {
        try {
            const response = await fetch(`http://localhost:3001/api/attendance/class/${curClass.class_id}`);
            const json = await response.json();
            setAtt(json);
        }catch{

        }
    }

    return(
        <>
            <ul>
                {att.map((item,id)=>{
                    return(
                        <li key={id} style={{display: "flex", borderBottom: "1px solid rgba(0,0,0,.125)", width: "600px"}}>
                            <p style={{marginBottom: "7px", marginTop: "7px", width: "400px", cursor: "default"}}>
                                {item.student_name}
                            </p>
                            <div style={{marginBottom: "7px", marginTop: "7px"}}>
                                {item.presence === null ?
                                    (<AddAttendance handleUpdated={()=>handleUpdated()} cur={item}/>)
                                    : item.presence === '1' ?
                                        (<i className="fas fa-plus" />) :
                                        (<i className="fas fa-minus" />)}</div>
                        </li>
                    )
                })}
            </ul>
        </>
    )
}
export default AttendanceByClass;