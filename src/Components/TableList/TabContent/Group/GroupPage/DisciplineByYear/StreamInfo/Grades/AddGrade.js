import React, {useState} from "react";
import {Form} from "react-bootstrap";

async function updateGrades(data){
    try {
        const response = await fetch(`http://localhost:3001/api/grades/${data.grade_id}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function AddGrade(props){
    const grade = props.grade;
    const [add, setAdd] = useState(false);
    const [g, setG] = useState("0");
    const [date, setDate] = useState("");

    function getReverse(s){
        let t = s.split(".");
        t = t.reverse();
        return t[0] + "-" + t[1] + "-" + t[2];
    }

    const handleAdd = () => {
        if (date.match(/^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((20)\d{2})\s*$/)) {
            const data = grade;
            data.grade = g;
            data.grade_date = getReverse(date);
            updateGrades(data).then(()=>{
                setAdd(false);
                props.handleUpdate();
            })
        }

    }
    return(
        <>
            {add ?
                (<>
                    <Form style={{display:"flex"}}>
                        <Form.Group controlId="grade">
                            <Form.Control
                                as="select"
                                type="text"
                                style={{width: "35px", marginRight: "90px", marginLeft: "-15px"}}
                                value={g}
                                onChange={(e)=>{setG(e.target.value)}}
                            >
                                <option value="0">0</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="date">
                            <Form.Control
                               type="text"
                                placeholder="Дата"
                                style={{width: "100px"}}
                                value={date}
                                onChange={(e)=>{
                                    setDate(e.target.value);
                                    //setYearMessage("");
                                }}
                            />
                        </Form.Group>

                    </Form>
                    <i style={{marginBottom: "7px",
                        marginTop: "7px",
                        cursor: "pointer",
                        marginRight: "7px",
                        marginLeft: "11px",
                        paddingTop:"6px"}} className="fas fa-check" onClick={handleAdd}/>
                </>) :
                (<>
                    <div style={{marginBottom: "7px", marginTop: "7px", marginRight: "60px", width: "60px"}}>
                        -
                    </div>
                    <p style={{marginBottom: "7px", marginTop: "7px", cursor: "default", width: "100px"}}>
                        -
                    </p>
                    <i style={{marginBottom: "7px",
                        marginTop: "7px",
                        cursor: "pointer",
                        marginRight: "10px",
                        paddingTop:"6px"}} className="fas fa-plus" onClick={()=>setAdd(true)}/>
                </>)}

        </>
    )
}
export default AddGrade;