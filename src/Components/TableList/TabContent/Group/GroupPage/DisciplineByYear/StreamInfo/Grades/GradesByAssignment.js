import React, {useEffect, useState} from "react";
import AddGrade from "./AddGrade";

function GradesByAssignment(props){
    const assignment = props.assignment;
    const [att, setAtt] = useState([]);
    const [updated, setUpdated] = useState(false);
    const handleUpdated = () => {
        setUpdated(true);
    }

    useEffect(()=>{
        if (att.length === 0){
            fetchGrades().then();
        } else
        if (updated){
            fetchGrades().then();
            setUpdated(false);
        }
    },[att.length, updated, assignment])

    function getReverse(s){
        let t = s.split("-");
        t = t.reverse();
        return t[0] + "." + t[1] + "." + t[2];
    }

    async function fetchGrades() {
        try {
            const response = await fetch(`http://localhost:3001/api/grades/plan/${assignment.plan_id}`);
            const json = await response.json();
            setAtt(json);
        }catch{

        }
    }
    return(
        <>
            <ul>
                {att.map((item,id)=>{
                    return(
                        <li key={id} style={{display: "flex", borderBottom: "1px solid rgba(0,0,0,.125)", width: "600px"}}>
                            <p style={{marginBottom: "7px", marginTop: "7px", width: "350px", cursor: "default"}}>
                                {item.student_name}
                            </p>
                            {item.grade === null ? (<AddGrade handleUpdate={()=>handleUpdated()} grade={item}/>) :
                                (
                                    <>
                                        <div style={{marginBottom: "7px", marginTop: "7px", marginRight: "60px", width: "60px"}}>
                                            {item.grade}
                                        </div>
                                        <p style={{marginBottom: "7px", marginTop: "7px", cursor: "default", width: "120px"}}>
                                            {item.grade_date ? getReverse(item.grade_date) : "-"}
                                        </p>
                                    </>
                                )}

                        </li>
                    )
                })}
            </ul>
        </>
    )
}
export default GradesByAssignment;