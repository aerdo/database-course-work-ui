import React from "react";
import {Col, ListGroup, Row, Tab} from "react-bootstrap";
import GradesByAssignment from "./GradesByAssignment";

function Grades(props){
    const assignments = props.assignments;

    function getReverse(s){
        let t = s.split("-");
        t = t.reverse();
        return t[0] + "." + t[1] + "." + t[2];
    }

    return(
        <div style={{marginTop: "30px"}}>
            <Tab.Container id="classes-tabs" defaultActiveKey="r0">
                <Row>
                    <Col md="auto" style={{height: "345px", width: "300px", overflowY: "auto", paddingRight: "0"}}>
                        <ListGroup>
                            {assignments.map((item,id)=>{
                                return(
                                    <ListGroup.Item key={id} action eventKey={"r"+id}
                                                    style={{paddingBottom: "0"}}>
                                        <p>{item.task_description} {getReverse(item.class_date)}</p>
                                    </ListGroup.Item>
                                )
                            })}
                        </ListGroup>
                    </Col>

                    <Col>
                        <Tab.Content style={{height: "345px",overflowY: "auto", marginBottom: "0"}}>
                            {assignments.map((item,id)=>{
                                return(
                                    <Tab.Pane key={id} eventKey={"r"+id} style={{margin: "0 30px 30px 10px"}}>
                                        <GradesByAssignment assignment={item}/>
                                    </Tab.Pane>
                                )
                            })}
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </div>
    )
}

export default Grades;