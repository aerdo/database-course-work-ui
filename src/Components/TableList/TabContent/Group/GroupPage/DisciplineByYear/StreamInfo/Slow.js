import {useEffect, useState} from "react";

function Slow(props){
    const streamId=props.streamId;
    const students = props.students;
    const [data, setData] = useState([]);
    const [avgs, setAvgs] = useState([]);

    async function fetchRating(studentId, streamId) {
        try {
            const response = await fetch(`http://localhost:3001/api/asses/${studentId}/${streamId}/count`)
            const json = await response.json();
            //setAvgs(json);
            return json;
        }catch{

        }
    }

    useEffect(() => {
        if (avgs.length === 0){
            let temp = [];
            students.forEach(i=>{
                fetchRating(i.student_id, streamId).then((res)=>{
                    const t = i;
                    if (res.get_count_n){
                        t.get_count_n = res.get_count_n;
                        temp.push(t);
                    }
                })
            });
            setData(temp);
        }

    }, [streamId, students,avgs.length]);

    return(
        <>
            <ul style={{marginTop: "20px", marginLeft: "-10px"}}>
                {data && data.map((item,id)=>{
                    return(
                        <li key={id} style={{listStyle: "none"}}>{id+1}. ({item.get_count_n}) {item.student_name} </li>
                    )
                })}
            </ul>
        </>
    )
}
export default Slow;