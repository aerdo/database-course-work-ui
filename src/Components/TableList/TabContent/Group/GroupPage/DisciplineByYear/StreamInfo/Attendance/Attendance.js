import {Col, ListGroup, Row, Tab} from "react-bootstrap";
import React from "react";
import AttendanceByClass from "./AttendanceByClass";

function Attendance(props){
    const classes = props.classes;

    function getReverse(s){
        let t = s.split("-");
        t = t.reverse();
        return t[0] + "." + t[1] + "." + t[2];
    }

    return(
        <div style={{marginTop: "30px"}}>
            <Tab.Container id="classes-tabs" defaultActiveKey="r0">
                <Row>
                    <Col md="auto" style={{height: "345px", width: "150px", overflowY: "auto", paddingRight: "0"}}>
                        <ListGroup>
                            {classes.map((item,id)=>{
                                return(
                                    <ListGroup.Item key={id} action eventKey={"r"+id}
                                                       style={{paddingBottom: "0"}}>
                                        <p>{getReverse(item.class_date)}</p>
                                    </ListGroup.Item>
                                )
                            })}
                        </ListGroup>
                    </Col>

                    <Col>
                        <Tab.Content style={{height: "345px", overflowY: "auto", marginBottom: "0"}}>
                            {classes.map((item,id)=>{
                                return(
                                    <Tab.Pane key={id} eventKey={"r"+id} style={{margin: "0 30px 30px 10px"}}>
                                        <AttendanceByClass class={item} />
                                    </Tab.Pane>
                                )
                            })}
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </div>
    )
}

export default Attendance;