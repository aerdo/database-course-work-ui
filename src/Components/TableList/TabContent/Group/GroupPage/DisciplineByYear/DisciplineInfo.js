import React, {useEffect, useState} from "react";
import {Button, Modal, Tab, Tabs} from "react-bootstrap";
import Attendance from "./StreamInfo/Attendance/Attendance";
import Grades from "./StreamInfo/Grades/Grades";
import Rating from "./StreamInfo/Rating";
import Slow from "./StreamInfo/Slow";

function DisciplineInfo(props){
    const discipline=props.discipline;
    const group=props.group;
    const [show, setShow] = useState(false);
    const [students, setStudents] = useState([]);
    const [classes, setClasses] = useState([]);
    const [assignments, setAssignments] = useState([]);

    async function fetchStudents() {
        try {
            const response = await fetch(`http://localhost:3001/api/groups/${group.group_id}`)
            const json = await response.json();
            setStudents(json);
        }catch{

        }
    }

    // получение занятий
    async function fetchClasses() {
        try {
            const response = await fetch(`http://localhost:3001/api/streams/${discipline.stream_id}/classes`);
            const json = await response.json();
            setClasses(json);
        }catch{

        }
    }

    async function fetchAssignments() {
        try {
            const response = await fetch(`http://localhost:3001/api/streams/${discipline.stream_id}/assignments`);
            const json = await response.json();
            setAssignments(json);
        }catch{

        }
    }

    const handleClose = () => {
        setShow(false);
    };
    const handleShow = () => {

        fetchStudents().then(()=>{
            fetchClasses().then(()=>{
                fetchAssignments().then();
            })
        })
        setShow(true);
    }
    return(
        <>
            <i className="far fa-eye" onClick={handleShow}/>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                size="xl"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem", paddingBottom: "0"}}>
                    <div style={{textAlign: "center"}}>
                        <h4>{discipline.discipline_name}</h4>
                        <p>{discipline.teacher_name}</p>
                    </div>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem", height: "430px"}}>
                    <Tabs defaultActiveKey="attendance">
                        <Tab eventKey="attendance" title="Посещаемость">
                            <Attendance groupId={group.group_id} classes={classes}/>
                        </Tab>
                        <Tab eventKey="grades" title="Оценки">
                            <Grades groupId={group.group_id} assignments={assignments}/>
                        </Tab>
                        <Tab eventKey="rating" title="Рейтинг">
                            <Rating students={students} streamId={discipline.stream_id}/>
                        </Tab>
                        <Tab eventKey="slow" title="Неуспевающие">
                            <Slow students={students} streamId={discipline.stream_id}/>
                        </Tab>
                    </Tabs>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addGroup" variant="outline-primary" onClick={handleClose}> Закрыть </Button>

                </Modal.Footer>
            </Modal>
        </>
    )
}

export default DisciplineInfo;