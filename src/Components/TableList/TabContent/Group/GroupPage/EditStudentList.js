import {Button, Form, Modal, Spinner, Table} from "react-bootstrap";
import React, {useState} from "react";
import {Typeahead} from "react-bootstrap-typeahead";

async function updateStudent(data){
    try {
        const response = await fetch(`http://localhost:3001/api/students/${data.student_id}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function EditStudentList(props){
    const students = props.students;
    const [show, setShow] = useState(false);
    const [load, setLoad] = useState(false);

    const [selectedStudent, setSelectedStudent] = useState([]);
    const [availableStudents, setAvailableStudents] = useState([]);

    async function fetchStudents() {
        try {
            const response = await fetch("http://localhost:3001/api/students/spare")
            const json = await response.json();
            setAvailableStudents(json);
        }catch{

        }
    }

    const handleClose = () => {
        setShow(false);

    }

    const handleShow = () => {
        setSelectedStudent([]);
        fetchStudents().then(()=>{
            setShow(true);
        });
    }

    const handleDelete = (item) => {
        const data = item;
        data.group_id = null;
        updateStudent(data).then(()=>{
            props.handleUpdated();
        })
    }

    const handleAdd = () => {
        setLoad(true);
        const data = {
            student_id: selectedStudent[0].student_id,
            student_name: selectedStudent[0].student_name,
            group_id: props.group.group_id
        }
        updateStudent(data).then(()=>{
            props.handleUpdated();
            setLoad(false);
        })
    }

    return(
        <>
            <Button variant="outline-dark" style={{marginLeft: "10px"}} onClick={handleShow}>
                <i className="fas fa-pen"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                /*style={{paddingTop: "4rem"}}*/
                size="lg"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Студенты группы {props.group.group_name}</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem", paddingBottom: "0"}}>
                    <Form style={{display: "flex", height: "62px"}}>
                        <Form.Group>
                            <Typeahead
                                disabled={availableStudents.length === 0}
                                id="set-student-group"
                                labelKey={option => `${option.student_name}`}
                                onChange={setSelectedStudent}
                                options={availableStudents}
                                placeholder="Студент"
                                selected={selectedStudent}
                                style={{width: "400px"}}
                            />
                            <Form.Text></Form.Text>
                        </Form.Group>
                        {load ?
                            (
                                <Button style={{height: "38px", marginLeft: "10px"}}>
                                    <Spinner
                                        as="span"
                                        animation="border"
                                        size="sm"
                                        role="status"
                                        aria-hidden="true"
                                    />
                                </Button>
                            ) :
                            (<Button style={{height: "38px", marginLeft: "10px"}} onClick={handleAdd}>Добавить</Button>)
                        }

                    </Form>
                    <Table style={{borderBottom: "2px solid black", marginBottom: "0"}}>
                        <thead>
                        <tr>
                            <th style={{width: "8%"}}>№</th>


                            <th >ФИО студента</th>
                            <th style={{width: "8%"}}/>
                        </tr>
                        </thead>
                    </Table>
                    <div style={{height: "340px", overflowY: "auto"}}>
                        <Table >
                            <tbody>
                            {students.map((item,id)=>{
                                return(
                                    <tr key={id}>
                                        <td style={{width: "8%"}}>{id+1}</td>

                                        <td>{item.student_name}</td>
                                        <td style={{width: "8%"}}>
                                            <div className="button-cross"
                                                 style={{color: "rgb(13 110 253 / 60%)"}}
                                                onClick={()=>handleDelete(item)}
                                            >
                                                <i className="fas fa-minus"/>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addStudent" variant="outline-primary" onClick={handleClose}>Закрыть</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default EditStudentList;