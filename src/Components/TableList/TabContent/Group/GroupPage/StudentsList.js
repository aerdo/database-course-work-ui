import React, {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import EditStudentList from "./EditStudentList";

function StudentsList(props){
    const groupName = props.group.group_name;
    const groupId = props.group.group_id;
    const [students, setStudents] = useState([]);

    const [updated, setUpdated] = useState(false);
    const handleUpdated = () => {
        setUpdated(true);
    }

    async function fetchStudents() {
        try {
            const response = await fetch(`http://localhost:3001/api/groups/${groupId}`)
            return await response.json();
        }catch{

        }
    }

    useEffect(() => {
        fetchStudents().then(res=>{
            if (res.length === 0){
                setStudents([]);
            }else{
                setStudents(res);
            }
        });
        if (updated){
            setUpdated(false);
        }
    },[groupId, updated]);

    return(
        <div style={{display: "inline-block", verticalAlign: "top"}}>
            <div style={{marginBottom: "20px", marginTop: "20px"}}>
                <h5 style={{display: "inline-block", verticalAlign: "middle", marginBottom: "0"}}>
                    Студенты группы {groupName}
                </h5>
                <EditStudentList students={students}  group={props.group} handleUpdated={()=>handleUpdated()}/>
            </div>

            <Table style={{width: "350px", borderBottom: "2px solid black", marginBottom: "0"}}>
                <thead>
                <tr>
                    <th style={{width: "8%"}}>№</th>
                    <th >ФИО студента</th>
                </tr>
                </thead>
            </Table>
            <div style={{height: "380px", overflowY: "auto", width: "350px"}}>
                <Table hover>
                    <tbody>
                    {students.map((item,id)=>{
                        return(
                            <tr key={id}>
                                    <td style={{width: "8%"}}>{id+1}</td>
                                    <td>{item.student_name}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>


        </div>
    );
}


export default StudentsList;