import {Accordion} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import DisciplineByYear from "./DisciplineByYear/DisciplineByYear";

function DisciplineList(props){
    const groupId = props.group.group_id;
    const [disciplines, setDisciplines] = useState([]);
    const [years, setYears] = useState([]);

/*    const [updated, setUpdated] = useState(false);
    const handleUpdated = () =>{
        setUpdated(true);
    }*/

    async function fetchDisciplines() {
        try {
            const response = await fetch(`http://localhost:3001/api/groups/${groupId}/disciplines`)
            return await response.json();
        }catch{

        }
    }

    /*<Button variant="outline-dark" style={{margin: "0 0 0 10px", display: "inline-block"}} >
                    <i className="fas fa-plus"/>
                </Button>*/

    useEffect(() => {
        fetchDisciplines().then((res)=>{
            if (res.length !== 0){
                let x = [];
                res.map(item=>x.push(item.stream_year));
                const y = Array.from(new Set(x));
                setYears(y);
                setDisciplines(res);
            }else{
                setDisciplines([]);
            }
        });

    },[groupId]);

    return(
        <div style={{display: "inline-block", verticalAlign: "top", marginLeft: "40px", marginTop: "20px", width: "619px"}}>
            <div style={{marginBottom: "20px"}}>
                <h5 style={{display: "inline-block", marginBottom: "0"}}>Дисциплины</h5>
            </div>
            {disciplines.length === 0 ? (
                <div>Идет набор студентов в группу. После завершения набора формируются потоки.</div>
            ) : (
                <div style={{height: "420px", overflowY: "auto"}}>
                    {years.map((item, id) => {
                        return(
                            <Accordion key={id} style={{margin: "5px 5px 10px 5px"}}>
                                <Accordion.Item eventKey={id.toString()}>
                                    <Accordion.Header>{item}</Accordion.Header>
                                    <Accordion.Body>
                                        <DisciplineByYear items={disciplines} year={item} group={props.group} />
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        )
                    })}
                </div>
            )}

        </div>
    )
}

export default DisciplineList;