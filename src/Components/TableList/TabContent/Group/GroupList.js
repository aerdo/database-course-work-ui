import {Button, Modal} from "react-bootstrap";
import React, {useState} from "react";

async function deleteGroup(groupId){
    try {
        const response = await fetch(`http://localhost:3001/api/groups/${groupId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function GroupList(props){
    const groups=props.groups;
    const [show, setShow] = useState(false);
    const [messageDelete, setMessageDelete] = useState("");

    const handleDelete = (group) =>{
        deleteGroup(group.group_id).then((res)=>{
            if (res && res.message){
                setMessageDelete("Невозможно удалить группу: "+ group.group_name);
                setTimeout(()=>setMessageDelete(""),1500);
            }else{
                setMessageDelete("");
                window.location.reload();
            }
        })
    }

    const handleClose = () => {
        setShow(false);
    };
    const handleShow = () => {
        setShow(true);
        setMessageDelete("");
    }
    return(
        <>
            <Button variant="outline-primary" style={{marginLeft:"10px"}} onClick={handleShow}>Список всех групп</Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Группы</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <ul>
                        {groups.map((item,id)=>{
                            return(
                                <li key={id} style={{listStyle: "none"}}>
                                    {item.group_name}
                                    <div className="button-cross"
                                         style={{display: "inline-block", verticalAlign: "top", marginLeft: "10px", color: "rgb(13 110 253 / 20%)"}}
                                         >
                                        <i className="fas fa-times" onClick={()=>handleDelete(item)}/>

                                    </div>

                                </li>
                            )
                        })}
                    </ul>
                    <p style={{marginLeft: "10px", color: "#0d6efd"}}>{messageDelete}</p>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addGroup" variant="outline-primary" onClick={handleClose}> Закрыть </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default GroupList;