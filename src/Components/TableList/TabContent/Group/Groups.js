import React, {useState, useEffect} from "react";
import {Button, Card, Form} from "react-bootstrap";
import AddGroup from "./AddGroup";
import StudentsList from "./GroupPage/StudentsList";
import DisciplineList from "./GroupPage/DisciplineList";
import GroupList from "./GroupList";

/*
<p>При выборе одной отображаются все её потоки</p>
<p>По каждому потоку просмотр и изменение посещаемости и успеваемости</p>
<p>Добавление в нее студентов без указанной группы</p>
<p>Расчет рейтинга студентов</p>
<p>Текущая успеваемость по дисциплине</p>
<p>Составление списка неуспевающих студентов</p>
*/

function Groups(){
    const [groups, setGroups] = useState([]);
    const [groupName, setGroupName] = useState("");
    const [groupToSend, setGroupToSend] = useState({});
    const [mes, setMes] = useState("");
    const [show, setShow] = useState(false);

    /*const [updated, setUpdated] = useState(false);
    const handleUpdated = () =>{
        setUpdated(true);
    }*/

    async function fetchGroups() {
        try {
            const response = await fetch("http://localhost:3001/api/groups")
            const json = await response.json();
            setGroups(json);
        }catch{

        }
    }

    useEffect(() => {
        if (groups.length === 0) {
            fetchGroups().then();
        }/*else if (updated){
            fetchGroups().then();
            setUpdated(false);
        }*/
    },[groups.length/*, updated*/]);


    const handleShowMes = () => {
        setShow(false);
        setGroupName(groupName.toUpperCase());
        if (groupName.length !== 0){
            const g = groups.find(i=>i.group_name === groupName.toUpperCase());
            if (!g) {
                setMes("Нет такой группы")
            }
            else {
                setGroupToSend(g);
                setShow(true);
            }
        }else{
            setMes("Введите название группы");
        }
    }

    return(
        <div>

            <div style={{marginBottom: "20px"}}>
                <h4 style={{display: "inline-block", verticalAlign: "top", marginBottom: "0"}}>Учебные группы</h4>
                <AddGroup /*handleUpdated={()=>handleUpdated()}*/ />
                <GroupList groups={groups} />
            </div>
            <Card body>
                <Form.Group style={{display: "inline-flex"}}>
                    <Form.Control type="text" placeholder="Группа" style={{width: "100px", marginRight: "10px"}} value={groupName}
                                  onChange={(e)=>{
                                      if (e.target.value.length <=10) {
                                          setGroupName(e.target.value);
                                          setMes("");
                                      }
                                  }}
                                  onKeyPress={(e)=> {
                                      if (e.key === "Enter" ) {
                                          e.preventDefault();
                                          handleShowMes();
                                      }
                                  }}
                    />

                    <Button onClick={handleShowMes} style={{marginRight: "10px"}}>Просмотр</Button>
                    <Button onClick={()=>window.location.reload()} variant="outline-primary"><i className="fas fa-sync-alt"/></Button>

                    <Form.Text style={{margin: "9px 0 0 10px"}}>
                        {mes}
                    </Form.Text>
                </Form.Group>
            </Card>
            <div>
                {show ? (
                    <div>
                        <StudentsList group={groupToSend}/>
                        <DisciplineList group={groupToSend}/>
                    </div>

                ) : (<div/>)}
            </div>
        </div>
    )
}

export default Groups;