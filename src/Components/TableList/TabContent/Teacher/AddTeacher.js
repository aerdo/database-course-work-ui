import React, {useState} from "react";
import {Button, Form, Modal, Spinner} from "react-bootstrap";

async function postData(data){
    try {
        const response = await fetch("http://localhost:3001/api/teachers/",{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return await response.json();
    }catch{
        //console.error("Error");
    }
}

function AddTeacher(props){
    const [teacherName, setTeacherName] = useState("");
    const [messageName, setMessageName] = useState("");

    const [show, setShow] = useState(false);
    const [load, setLoad] = useState(false);

    const handleClose = () => {
        setShow(false);
        setTeacherName("");
        setMessageName("");
    };
    const handleShow = () => {
        setShow(true);
    }

    const handleLoad = () => {
        setLoad(true);
        handleCheck();
    }

    const handleCheck = () => {
        if (! teacherName ) {
            setMessageName("Поле не должно быть пустым");
            setLoad(false);
        } else {
            setMessageName("");
        }

        if (teacherName){
            setMessageName("");
            const data = {
                teacher_name: teacherName
            }
            postData(data).then(()=>{
                setMessageName("");
                setLoad(false);
                handleClose();
                //window.location.reload();
                props.handleUpdated();
            });
        }
    }

    return(
        <>
            <Button variant="outline-dark" style={{margin: "0 0 0 20px"}} onClick={handleShow}>
                <i className="fas fa-plus"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Добавить преподавателя</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="addTeacher" >
                        <Form.Group style={{marginBottom: "20px"}}>
                            <Form.Control type="text" placeholder="ФИО преподавателя" value={teacherName}
                                          onChange={(e)=>{
                                              setTeacherName(e.target.value);
                                              setMessageName("");
                                          }}
                                          onKeyPress={(e)=> {
                                              if (e.key === "Enter" ) {
                                                  e.preventDefault();
                                                  handleLoad();
                                              }
                                          }}
                            />
                            <Form.Text>
                                {messageName}
                            </Form.Text>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addTeacher" variant="outline-primary" onClick={handleClose}> Отмена </Button>
                    {load ?
                        (
                            <Button variant="primary">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                <span className="visually-hidden">Loading...</span>
                            </Button>
                        ) :
                        (<Button form="addTeacher" variant="primary" onClick={handleLoad}>Добавить</Button>)
                    }
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddTeacher;
