import React, {useState, useEffect} from "react";
import {Table} from "react-bootstrap";
import AddTeacher from "./AddTeacher";
import TeacherReview from "./TeacherReview";


function Teachers(){
    const [teachers, setTeachers] = useState([]);
    const [updated, setUpdated] = useState(false);

    const handleUpdated = () =>{
        setUpdated(true);
    }

    async function fetchTeachers() {
        try {
            const response = await fetch("http://localhost:3001/api/teachers")
            const json = await response.json();
            setTeachers(json);
        }catch{

        }
    }

    useEffect(() => {
        if (teachers.length === 0) fetchTeachers().then();
        else if (updated){
            setUpdated(false);
            fetchTeachers().then();
        }
    },[teachers.length, updated]);

    return(
        <div >
            <div style={{marginBottom: "20px"}}>
                <h4 style={{display: "inline-block", verticalAlign: "top", marginBottom: "0"}}>Преподаватели</h4>
                <AddTeacher handleUpdated = {()=>handleUpdated()}/>
            </div>

            <Table style={{width: "600px", borderBottom: "3px solid black", marginBottom: "0"}}>
                <thead>
                <tr>
                    <th style={{width: "8%"}}>id</th>
                    <th >ФИО преподавалетя</th>
                </tr>
                </thead>
            </Table>

            <div style={{height: "530px", overflowY: "auto", width: "600px"}}>
                <Table hover>
                    <tbody>
                    {teachers.map((item,id)=>{
                        return(
                            <TeacherReview teacher={item} key={id} handleUpdated = {()=>handleUpdated()}/>
                        )
                    })}
                    </tbody>
                </Table>
            </div>


        </div>
    );
}

export default Teachers;