import React, {useEffect, useState} from "react";
import AddDiscipline from "./AddDiscipline";
import {Col, ListGroup, Row, Tab} from "react-bootstrap";
import TaskList from "./Task/TaskList";

function Disciplines(){
    const [disciplines, setDisciplines] = useState([]);
    const [updated, setUpdated] = useState(false);

    const handleUpdated = () =>{
        setUpdated(true);
    }

    async function fetchDisciplines() {
        try {
            const response = await fetch("http://localhost:3001/api/disciplines")
            const json = await response.json();
            setDisciplines(json);
        }catch{

        }
    }

    useEffect(() => {
        if (disciplines.length === 0) fetchDisciplines().then();
        else if (updated){
            setUpdated(false);
            fetchDisciplines().then();
        }
    }, [disciplines.length, updated]);

    return(
        <div>
            <div style={{marginBottom: "20px"}}>
                <h4 style={{display: "inline-block", verticalAlign: "top", marginBottom: "0"}}>Дисциплины</h4>
                <AddDiscipline handleUpdated = {()=>handleUpdated()}/>
            </div>


            <div style={styles.contentContainer}>
                <Tab.Container id="list-group-tabs" /*defaultActiveKey="r0"*/>
                    <Row>
                        <Col md="auto" style={{width: "290px", height: "540px", overflowY: "auto", overflowX: "hidden", paddingRight: "0"}}>
                            <ListGroup style={styles.listGroup} variant="flush">
                                {disciplines.map((item,id)=>{
                                    return(
                                        <ListGroup.Item key={id} action eventKey={"r"+id}
                                            style={{paddingBottom: "0"}}>
                                            <p>{item.discipline_name}</p>
                                        </ListGroup.Item>
                                    )
                                })}
                            </ListGroup>
                        </Col>

                        <Col>
                            <Tab.Content style={{height: "540px", overflowY: "auto", marginBottom: "0"}}>
                                {disciplines.map((item,id)=>{
                                    return(
                                        <Tab.Pane key={id} eventKey={"r"+id} style={{margin: "0 30px 30px 10px"}}>
                                            <div>
                                                <TaskList discipline = {item} handleUpdated={()=>handleUpdated()}/>
                                            </div>
                                        </Tab.Pane>
                                    )
                                })}
                            </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>
            </div>
        </div>
    )
}

const styles = {
    contentContainer: {
        width: "1000px"
    },
    listGroup: {
        width: "270px",
    }
}


export default Disciplines;