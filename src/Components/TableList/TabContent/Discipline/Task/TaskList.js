import React, {useState, useEffect} from "react";
import UpdateDiscipline from "../UpdateDiscipline";
import AddTask from "./AddTask";

async function deleteTask(disciplineId, taskId){
    try {
        const response = await fetch(`http://localhost:3001/api/disciplines/tasks/${taskId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

function TaskList(props){
    const discipline = props.discipline;
    const [tasks, setTasks] = useState([]);

    const [updated, setUpdated] = useState(false);

    const handleUpdatedDisciplines = () =>{
        props.handleUpdated();
    }

    const handleUpdated = () =>{
        setUpdated(true);
    }

    async function fetchTasks() {
        try {
            const response = await fetch(`http://localhost:3001/api/disciplines/${discipline.discipline_id}/tasks`)
            const json = await response.json();
            setTasks(json);
        }catch{
            //console.error("Error getting tasks");
        }
    }

    const handleDelete = (disciplineId, taskId) =>{
        deleteTask(disciplineId, taskId).then((res)=>{
            if (res && res.message){

            }else{
                handleUpdated();
            }
        })
    }

    useEffect(() => {
        if (tasks.length === 0) fetchTasks().then();
        else if (updated){
            setUpdated(false);
            fetchTasks().then();
        }
    }, [tasks.length, updated]);

    return(
        <div>
            <div style={{marginBottom: "20px", marginTop: "10px", marginLeft: "15px"}}>
                <h4 style={{display: "inline-block", verticalAlign: "top", marginBottom: "0"}}>Задания</h4>
                <AddTask handleUpdated={()=>handleUpdated()} discipline={discipline}/>
            </div>
            <ul>
            {tasks.map((item,id)=>{
                return(
                    <li key={id}>
                        <div style={{display: "inline-block", verticalAlign: "top"}}>
                            {item.task_description}

                        </div>
                        <div style={{display: "inline-block", verticalAlign: "top", marginLeft: "10px", color: "#0d6efd"}}>
                            ({  DICT.find(i=>i.letter === item.task_type).meaning })
                        </div>

                        <div className="button-cross"
                             style={{display: "inline-block", verticalAlign: "top", marginLeft: "10px", color: "rgb(13 110 253 / 20%)"}}
                        onClick={()=>handleDelete(item.discipline_id, item.task_id)}>
                            <i className="fas fa-times"/>
                        </div>
                    </li>
                )
            })}
            </ul>
            <UpdateDiscipline handleUpdated={()=>handleUpdatedDisciplines()} discipline={discipline}/>

        </div>
    )
}

const DICT = [
    {
        letter: "l",
        meaning: "Лаб"
    },
    {
        letter: "p",
        meaning: "Практ"
    },
    {
        letter: "t",
        meaning: "ТР"
    },
    {
        letter: "k",
        meaning: "КП"
    },
    {
        letter: "c",
        meaning: "КР"
    }
]

export default TaskList;