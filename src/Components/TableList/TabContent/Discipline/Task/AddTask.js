import React, {useState} from "react";
import {Button, Form, Modal, Spinner} from "react-bootstrap";

async function postData(data){
    try {
        const response = await fetch(`http://localhost:3001/api/disciplines/tasks/`,{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return await response.json();
    }catch{
        //console.error("Error");
    }
}


function AddTask(props){
    const discipline=props.discipline;

    const [taskDescription, setTaskDescription] = useState("");
    const [taskType, setTaskType] = useState("l");

    const [messageDescription, setMessageDescription] = useState("");

    const [show, setShow] = useState(false);
    const [load, setLoad] = useState(false);

    const handleClose = () => {
        setShow(false);
        setTaskDescription("");
        setMessageDescription("");
        setTaskType("l");
    };

    const handleShow = () => {
        setShow(true);
    }

    const handleLoad = () => {
        setLoad(true);
        handleCheck();
    }

    const handleCheck = () => {
        if (! taskDescription ) {
            setMessageDescription("Поле не должно быть пустым");
            setLoad(false);
        } else {
            setMessageDescription("");
        }

        if (taskDescription){
            setMessageDescription("");
            const data = {
                discipline_id: discipline.discipline_id,
                task_type: taskType,
                task_description: taskDescription
            };

            setLoad(false);
            postData(data).then(()=>{
                setMessageDescription("");
                setLoad(false);
                handleClose();
                props.handleUpdated();
            });
        }
    }

    return(
        <>
            <Button variant="outline-primary" style={{margin: "0 0 0 20px"}} onClick={handleShow}>
                <i className="fas fa-plus"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Новое задание</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="addTask" >
                        <Form.Group style={{marginBottom: "20px"}}>
                            <Form.Control type="text" placeholder="Описание задания" value={taskDescription}
                                          onChange={(e)=>{
                                              setTaskDescription(e.target.value);
                                              setMessageDescription("");
                                          }}
                                          onKeyPress={(e)=> {
                                              if (e.key === "Enter" ) {
                                                  e.preventDefault();
                                                  handleLoad();
                                              }
                                          }}
                            />
                            <Form.Text>
                                {messageDescription}
                            </Form.Text>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control as="select" value={taskType} onChange={e=>{
                                setTaskType(e.target.value)}}>
                                <option value="l">Лабораторная работа</option>
                                <option value="p">Практическое задание</option>
                                <option value="t">Типовой расчет</option>
                                <option value="k">Курсовой проект</option>
                                <option value="c">Контрольная работа</option>
                            </Form.Control>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addTask" variant="outline-primary" onClick={handleClose}> Отмена </Button>
                    {load ?
                        (
                            <Button variant="primary">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                <span className="visually-hidden">Loading...</span>
                            </Button>
                        ) :
                        (<Button form="addTask" variant="primary" onClick={handleLoad}>Добавить</Button>)
                    }
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddTask;