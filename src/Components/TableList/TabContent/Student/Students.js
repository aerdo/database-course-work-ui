import React, {useEffect, useState} from "react";
import {Button, Table} from "react-bootstrap";
import AddStudent from './AddStudent';
import StudentReview from "./StudentReview";

function Students(){
    const [groups, setGroups] = useState([]);
    const [students, setStudents] = useState([]);
    /*const [updated, setUpdated] = useState(false);*/

    /*const handleUpdated = () =>{
        //setUpdated(true);
        window.location.reload();
    }*/

    async function fetchStudents() {
        try {
            const response = await fetch("http://localhost:3001/api/students")
            const json = await response.json();
            setStudents(json);
        }catch{

        }
    }

    async function fetchGroups() {
        try {
            const response = await fetch("http://localhost:3001/api/groups")
            const json = await response.json();
            setGroups(json);
        }catch{

        }
    }

    useEffect(() => {
        if (students.length === 0) {
            fetchGroups().then(()=>{
                fetchStudents().then();
            });

        }
        /*else if (updated){
            setUpdated(false);
            fetchStudents().then();

        }*/
    },[students.length/*, updated*/]);

    return(
        <div >
            <div style={{marginBottom: "20px"}}>
                <h4 style={{display: "inline-block", verticalAlign: "top", marginBottom: "0"}}>Студенты</h4>
                <AddStudent /*handleUpdated = {()=>handleUpdated()}*/ />
                <Button onClick={()=>window.location.reload()} variant="outline-primary"><i className="fas fa-sync-alt"/></Button>
            </div>

            <Table style={{width: "600px", borderBottom: "3px solid black", marginBottom: "0"}}>
                <thead>
                <tr>
                    <th style={{width: "8%"}}>id</th>
                    <th >ФИО студента</th>
                    <th style={{width: "22%"}}>Группа</th>
                </tr>
                </thead>
            </Table>

            <div style={{height: "530px", overflowY: "auto", width: "600px"}}>
                <Table hover>
                    <tbody>
                    {students.map((item,id)=>{
                        let group = item.group_id ? groups.find(i=>i.group_id === item.group_id).group_name : "-"
                        return(
                            <StudentReview group={group} student={item} key={id} /*handleUpdated = {()=>handleUpdated()}*/ />
                        )
                    })}
                    </tbody>
                </Table>
            </div>


        </div>
    );
}

export default Students;