import React, {useState} from "react";
import {Button, Form, Modal, Spinner} from "react-bootstrap";

async function deleteStudent(studentId){
    try {
        const response = await fetch(`http://localhost:3001/api/students/${studentId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        //console.error("Error!");
    }
}

async function updateStudent(studentId, data){
    try {
        const response = await fetch(`http://localhost:3001/api/students/${studentId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        const json = await response.json();
        console.log(JSON.stringify(json));
        return json;
    }catch{
        //console.error("Error!");
    }
}

function StudentReview(props){
    const [show, setShow] = useState(false);
    const student = props.student;
    const group = props.group;

    const [name, setName] = useState(student.student_name);
    const [messageName, setMessageName] = useState("");

    const [update, setUpdate] = useState(false);
    const [check, setCheck] = useState(false);
    const [load, setLoad] = useState(false);

    const [messageDelete, setMessageDelete] = useState("");

    const handleClose = () => {
        setMessageName("");
        setUpdate(false);
        setCheck(false);
        setShow(false);
    }

    const handleShow = () => {
        setShow(true);

        setName(student.student_name);
        setMessageName("");

        setUpdate(false);
        setCheck(false);
        setLoad(false);
    }

    const handleDelete = () =>{
        deleteStudent(student.student_id).then(res=>{
            if (res && res.message){
                setMessageDelete("Невозможно удалить студента");
                setTimeout(()=>setMessageDelete(""),1500);
            }else{
                handleClose();
                //props.handleUpdated();
                window.location.reload();
            }
        })
    }

    const handleLoadUpdate = () => {
        setLoad(true);
        if (!name){
            setMessageName("Поле не должно быть пустым")
        }else{
            if (name === student.student_name){
                setMessageName("");
                handleClose();
            }else{
                setMessageName("");
                const data = {
                    student_name: name,

                }
                updateStudent(student.student_id, data).then(res=>{
                    setLoad(false);
                    if (res.message) {
                        if (res.message){
                            setMessageName("Ошибка");
                            setUpdate(true);
                            setCheck(false);
                        }
                    } else {
                        student.student_name = name;
                        handleClose();
                        //props.handleUpdated();
                        window.location.reload();
                    }
                })
            }
        }
    }

    const handleCancel = ()=>{
        setName(student.student_name);
        setMessageName("");

        setUpdate(false);
        setCheck(false);
        setLoad(false);
    }

    return(
        <>
            <tr onClick={handleShow}>
                <td style={{width: "8%"}}>{student.student_id}</td>
                <td>{student.student_name}</td>
                <td style={{width: "20%"}}>{group}</td>
            </tr>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Студент</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem", paddingBottom: "0"}}>
                    <div>
                        <Form.Group
                            style={!(!update || load) ? {display: "inline-block",
                                verticalAlign: "top",
                                marginRight:"10px",
                                width: "290px"
                            }: {display: "inline-block",
                                verticalAlign: "top",
                                marginRight:"10px",
                                width: "345px"}}
                        >
                            <Form.Control type="text" placeholder="ФИО студента"
                                          value={name}
                                          readOnly={!update || load}
                                          onChange={(e)=>{
                                              setName(e.target.value);
                                              setMessageName("");
                                          }}
                            />
                            <Form.Text style={{marginTop: "0"}}>
                                {update ? (messageName) : (<></>)}
                            </Form.Text>
                        </Form.Group>

                        <div style={{display: "inline-block", verticalAlign: "middle"}}>
                            {
                                !update ?
                                    (<Button variant="primary"
                                             title="Редактировать"
                                             onClick={()=>setUpdate(true)}>
                                        <i className="fas fa-pen"/>
                                    </Button>) :

                                    !load ? (
                                        <>
                                            <Button variant="dark" disabled={check} title="Сохранить изменения"
                                                    onClick={handleLoadUpdate}>
                                                <i className="fas fa-check"/>
                                            </Button>
                                            <Button variant="outline-primary"
                                                    title="Отмена"
                                                    onClick={handleCancel}
                                                    style={{display: "inline-block", width: "42px", marginTop: "0", marginLeft: "10px"}}>
                                                <i className="fas fa-times"/>
                                            </Button>
                                        </>



                                    ) : (
                                        <Button variant="dark">
                                            <Spinner
                                                as="span"
                                                animation="border"
                                                size="sm"
                                                role="status"
                                                aria-hidden="true"
                                            />
                                            <span className="visually-hidden">Loading...</span>
                                        </Button>)
                            }
                        </div>
                    </div>
                    <div style={{display: "inline-flex", fontSize: "13px", marginTop: "10px"}}>
                        <p style={{cursor: "pointer", color: "rgb(13 110 253 / 50%)"}} onClick={handleDelete}>
                            Удалить студента
                        </p>
                        <p style={{marginLeft: "10px", color: "#0d6efd"}}>{messageDelete}</p>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addStudent" variant="outline-primary" onClick={handleClose}>Закрыть</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default StudentReview;
