import Teachers from "./TabContent/Teacher/Teachers";
import Groups from "./TabContent/Group/Groups";
import Streams from "./TabContent/Stream/Streams";
import Disciplines from "./TabContent/Discipline/Disciplines";
import Students from "./TabContent/Student/Students";

/*
    id: 1 title: "Преподаватели"
    id: 2 title: "Дисциплины"
    id: 3 title: "Студенты"
    id: 4 title: "Группы"
    id: 5 title: "Потоки"
*/

function ComponentSelector(props){
    const id = props.id;
    switch (id){
        case 1:
            return(<Teachers/>);
        case 2:
            return(<Disciplines />);
        case 3:
            return (<Students/>);
        case 4:
            return (<Groups />);
        case 5:
            return (<Streams/>);
        default:
            return (<h6>Такого не завезли</h6>);
    }
}

export default ComponentSelector;