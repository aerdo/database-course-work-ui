import React from "react";
import {Col, ListGroup, Row, Tab} from "react-bootstrap";
import ComponentSelector from "./ComponentSelector";

function TableList(){
    const handleChange = (id) =>{
        const s = id.toString();
        localStorage.setItem('cur',s);
    }
    const cur = localStorage.getItem('cur')
    return(
        <div style={styles.contentContainer}>
            <Tab.Container id="list-group-tabs" defaultActiveKey={"r"+cur} >
                <Row>
                    <Col sm={2}>
                        <ListGroup style={styles.listGroup} >
                            {TABLES.map((item,id)=>{
                                return(
                                    <ListGroup.Item key={id} action eventKey={"r"+id} onClick={()=>handleChange(id)}>
                                        <p style={styles.pGroup}>{item.title}</p>
                                    </ListGroup.Item>
                                )
                            })}
                        </ListGroup>
                    </Col>

                    <Col>
                        <Tab.Content>
                            {TABLES.map((item,id)=>{
                                return(
                                    <Tab.Pane key={id} eventKey={"r"+(item.id-1)} style={{margin: "0 30px 0 40px"}}>
                                        <div>
                                            <ComponentSelector id ={item.id} />
                                        </div>
                                    </Tab.Pane>
                                )
                            })}
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </div>
    );
}

const styles = {
    contentContainer: {
        width: "1300px",
        margin: "auto",
        marginTop: "60px"
    },
    listGroup: {
        width: "220px"
    },
    pGroup: {
        marginBottom: "0px"
    }
}

const TABLES = [
    {
        id: 1,
        title: "Преподаватели",
        description: "",
        link: ""
    },
    {
        id: 2,
        title: "Дисциплины",
        description: "",
        link: ""
    },
    {
        id: 3,
        title: "Студенты",
        description: "",
        link: ""
    },
    {
        id: 4,
        title: "Группы",
        description: "",
        link: ""
    },

    {
        id: 5,
        title: "Потоки",
        description: "",
        link: ""
    }
];

export default TableList;


