import {Navbar, Container, Nav} from "react-bootstrap";

function NavBar(){
    return(
        <Navbar bg="light" sticky="top">
            <Container>
                <Nav className="me-auto">
                    <Nav.Link href="/">Таблицы</Nav.Link>
                    <Nav.Link>Функции</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    )
}
export default NavBar;