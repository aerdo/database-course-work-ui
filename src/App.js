import React from "react";
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import './App.css';
import Tables from "./Pages/Tables";

//<NavBar/>

function App() {
  return (
      <>
          <Router>
              <Switch>
                  <Route exact path="/" component={Tables}/>
              </Switch>
          </Router>
      </>

  );
}

export default App;
